<?php echo $a->php; ?>

/**
 * Check to see if $user has the $op access on $entity.
 *
* If the entity is null, check if user has $op access on entity type or bundle.
 */
function <?php echo $a->s; ?>_entity_access($op, $bundle = NULL, $entity = NULL, $user = NULL) {
  $access_controlled_actions = <?php echo $a->access_controlled_actions ?>;
  $machine_name = <?php echo $a->m ?>;

  if (!in_array($op, $access_controlled_actions) && $op !== "administer $machine_name") {
    return FALSE;
  }

  $account = NULL;
  if ($user !== NULL && is_object($user)) {
    $account = $user;
  }
  elseif ($user !== NULL && ctype_digit($user)) {
    $account = user_load($user);
  }
  elseif ($user === NULL) {
    $account = $GLOBALS['user'];
  }
  else {
    throw new RuntimeException('can not determine user');
  }

  if($account->uid === 1) {
    return TRUE;
  }

  if (user_access("administer $machine_name", $account)) {
    return TRUE;
  }

  if (user_access("$machine_name - $op", $account)) {
    return TRUE;
  }

  if ($bundle !== NULL && user_access("$machine_name - $bundle - $op", $account)) {
    return TRUE;
  }

<?php if($a->has_owner->value): ?>
  $self = $entity && $account->uid === $entity-><?php echo $a->owner_key_name; ?>;
  if ($self && user_access("own - $machine_name - $op", $account)) {
    return TRUE;
  }

  if ($self && $bundle !== NULL && user_access("own - $machine_name - $bundle - $op", $account)) {
    return TRUE;
  }
<?php endif; ?>

  return FALSE;
}

/**
 * Implements hook_permission().
 */
function <?php echo $a->m; ?>_permission() {
  $machine_name = '<?php echo $a->m; ?>';
  $actions = <?php echo $a->access_controlled_actions; ?>;

  $bundles = <?php echo $a->s; ?>_get_bundles_names();

  $permissions = ["administer $machine_name"];

  foreach($actions as $op) {
    $permissions[] = "$machine_name - $op";
<?php if($a->has_owner->value): ?>
    $permissions[] = "own - $machine_name - $op";
<?php endif; ?>
  }

  foreach($actions as $op) {
    foreach(<?php echo $a->s; ?>_get_bundles_names() as $bundle) {
      $permissions[] = "$machine_name - $bundle - $op";
<?php if($a->has_owner->value): ?>
      $permissions[] = "own - $machine_name - $bundle - $op";
<?php endif; ?>
    }
  }

  return $permissions;
}


function <?php echo $a->s; ?>_bundle_access($op, $bundle = NULL, $entity = NULL, $user = NULL) {
  return user_access("administer $machine_name", $account);
}
